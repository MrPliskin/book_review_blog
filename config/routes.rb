Rails.application.routes.draw do
  resources :subscribers
  get 'about' => 'static_pages#about', as: :about
  get 'contact' => 'static_pages#contact', as: :contact

  get 'errors/not_found'

  get 'errors/internal_server_error'

  root 'blog_posts#index'

  '''
  GET    /blog_posts          blog_posts#index
  POST   /blog_posts          blog_posts#create
  GET    /blog_posts/new      blog_posts#new
  GET    /blog_posts/:id/edit blog_posts#edit
  GET    /blog_posts/:id      blog_posts#show
  PATCH  /blog_posts/:id      blog_posts#update
  PUT    /blog_posts/:id      blog_posts#update
  DELETE /blog_posts/:id      blog_posts#destroy
  '''
  resources :blog_posts, :except => [:about, :contact, :browse]
  get 'browse' => 'blog_posts#browse', as: :browse

  devise_for :users
  resources :users, :except => [:create, :new]

  post 'create_user' => 'users#create', as: :create_user
  get 'new_user' => 'users#new', as: :new_user

  resources :tags

  resources :authors

  resources :static_pages

  resources :subscribers, :except => [:new, :show, :edit, :index, :update, :destroy]

  get 'unsubscribe/:token' => 'subscribers#destroy', as: :unsubscribe

  # via all means any type of request(get, post, etc.)
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

end
