class AddAdditionalInfoToBlogPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :blog_posts, :info, :string
  end
end
