class AddNameToStaticPage < ActiveRecord::Migration[5.0]
  def change
    add_column :static_pages, :name, :string
  end
end
