class AddRatingToBlogPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :blog_posts, :rating, :int
  end
end
