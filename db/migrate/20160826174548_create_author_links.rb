class CreateAuthorLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :author_links do |t|
      t.string :link

      t.timestamps
    end
  end
end
