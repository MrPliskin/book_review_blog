class AddBookCoverToBlogPosts < ActiveRecord::Migration[5.0]
  def self.up
    add_attachment :blog_posts, :book_cover
  end

  def self.down
    remove_attachment :blog_posts, :book_cover
  end
end
