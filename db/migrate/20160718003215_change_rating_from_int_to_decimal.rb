class ChangeRatingFromIntToDecimal < ActiveRecord::Migration[5.0]
  def change
    change_column :blog_posts, :rating, :decimal
  end
end
