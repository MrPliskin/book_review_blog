require 'test_helper'

class BlogPostsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get root_path
    assert_response :success, "could not get index page"
  end

  test "should get new" do
    get new_blog_post_path
    assert_response :success, "could not get new blog post page"
  end
end
