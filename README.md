# README

This is a repo for a book review blog I'm creating for a friend.

The latest "production" version can be seen here: https://dashboard.heroku.com/apps/bibliotheque-maison-32637

***

## Resources Used

### [Ruby on Rails](http://rubyonrails.org/)

The full stack framework I used. I had previously used mostly PHP and some Django, but I chose to learn Rails for this
project because of the lack of either PHP or especially Django jobs in the area where I live.

### [Rails Tutorial](https://www.railstutorial.org/book/beginning)

This is where I learned the basics of Rails. I only went up to chapter 8, but I found the book extremely useful.

### [Stackoverflow](http://stackoverflow.com/)

Obviously. Is there a single project that could be achieved without this site? I wonder. Felt like it deserved a shoutout though.

### [Twitter Bootstrap](http://getbootstrap.com/)

The framework powering the majority of the CSS and Javascript used on the site.

### [Clean Blog](http://startbootstrap.com/template-overviews/clean-blog)

Initial template for the site was taken from Clean Blog v1.1.0.

It has since been modified substantially, although the general layout remains similar.

### [Devise](https://github.com/plataformatec/devise)

Rather than create my own authentication system, I used Devise. Very simple, up and running in about 10 minutes,
although there was much more tweaking to be done after that.

### [Summernote](http://summernote.org/)

The editor used for post creation. It has a variety of features, is relatively easy to set up, and best of all, it's free.

### [RateYo](http://rateyo.fundoocode.ninja/)

A Jquery plugin for the star rating system. Easily customizable and didn't take too much trouble to set up. Highly recommended.

### [truncate_html](https://github.com/ianwhite/truncate_html)

Small Rails helper that sanitizes html for post-preview purposes. I had previously had a problem where, due to truncation,
HTML tags could be left open. This helper sanitizes the HTML properly.

### [jquery-turbolinks](https://github.com/kossnocorp/jquery.turbolinks)

Jquery and turbolinks do not play nice. Without this gem, Jquery will often not run on pages navigated to via internal links.
Honestly, considering how ubiquitous Jquery is, I'm a bit shocked that turbolinks, a core Rails gem, has this issue with it.
jquery-turbolinks is a must-have gem in my opinion.

### [Tagging tutorial](http://tutorials.jumpstartlab.com/projects/blogger.html#i3:-tagging)

### [friendly_id](https://github.com/norman/friendly_id)

***

## TODO

### Post Creation

* Add place to add link to GoodReads; pull book cover image and relevant info from there
* Modify summernote editor to upload images properly
* ~~Make star rating optional~~
* Add (optional) links to amazon, facebook, pinterest, etc.
* ~~Add ability to tag articles~~

### Post display

* ~~Do not display rating if the post doesn't have one~~
* ~~Include book cover and author as part of post title on both index and show pages~~
* ~~Change date to more readable format~~
* ~~Display (optional) links to amazon, facebook, pinterest, etc.~~
* ~~Remove links to edit/delete if user is not logged in~~
* ~~Make posts on index page display in correct order (from most recent to least recent)~~
* ~~Limit index page to display only 10 post previews at a time~~
* ~~Make date region specific (Don't just display UTC for everyone)~~

### Nav Bar

* Experiment with new color scheme/layout
* ~~Don't display log out link unless user is logged in~~

### Other

* Add search cabability
* Add ability to delete users
* Setup support email account (ie. make support@whatever.herokuapp.com actually work)
* ~~Maybe switch about and contact pages to not be in the blog posts controller, also flesh them out~~
* Add tests
* Change spoiler tags to be like goodreads
* Refactor code into partials and whatnot, split css into files (Partially done)
* ~~Make stuff responsive to mobile~~
* Unsure about entire tag list on tag show page
* Allow selection of multiple tags
* ~~Organize tags by color, corresponding to type of tag(author, genre, other)~~
* Organize external links into groups that popup on click. Ie. share button containing links to facebook, twitter, etc
* ~~Allow people to subscribe to new posts via email~~
* Ads
* ~~Links to author's site~~
* ~~Add post count to tags~~
* ~~Add spoiler tags~~
* ~~Make tags prettier~~
* ~~Set up 404 page~~
* ~~Customize password reset email~~
* ~~Create contact and about pages~~
* ~~Add user authentication~~
* ~~Change URLs to be human readable (ie. example.com/example-article instead of example.com/14)~~
* Phrasing gem

### Bugs
* ~~You can't delete a post unless you delete tags first~~ (Fixed through dependent: :destroy)
* ~~Navbar brand is not vertically centered on mobile~~
* ~~When editing a post, the rating field is only automatically filled if the rating is a _.5, not if it's a whole number~~
