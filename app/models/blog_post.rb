# require 'uri'

class BlogPost < ApplicationRecord
  attr_accessor :image_url
  attr_accessor :author_name

  before_create :book_cover_from_url
  before_create :author_from_author_name

  before_update :book_cover_from_url
  before_update :author_from_author_name

  has_attached_file :book_cover, styles: {
    index: '200x300>',
    full: '314x475>'
  }

  def book_cover_from_url
    if image_url != ""
      self.book_cover = URI.parse(image_url)
    end
  end

  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :book_cover, :content_type => /\Aimage\/.*\Z/

  self.per_page = 6

  extend FriendlyId
  friendly_id :title, use: [:slugged, :history, :finders]

  def should_generate_new_friendly_id? #will change the slug if the name changed
    slug.blank? || title_changed?
  end

  validates :title, presence: true
  validates :body, presence: true # , length: { maximum: 40000 }
  # REMEMBER TO UPDATE CONTROLLER TO ALLOW NEW FIELDS
  # validates :rating, numericality: { only_decimal: true }

  # enables us to access all tags for a blog post through blog_post.tags
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings

  def tag_list
    self.tags.collect do |tag|
      # otherwise returns an array of tag objects, not tag names
      tag.name
    end.join(", ")
  end

  def tag_list=(tags_string)
    tag_names = tags_string.split(",").collect{|s| s.strip.downcase}.uniq
    new_or_found_tags = tag_names.collect { |name| Tag.find_or_create_by(name: name) }
    self.tags = new_or_found_tags
  end

  belongs_to :author
  
  def author_from_author_name
    if author_name != ""
      self.author_id = Author.find_or_create_by(name: author_name).id
      # self.author_name = Author.find(self.author_id).name
    end
  end
  
end
