class Author < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [:slugged, :history, :finders]

  def should_generate_new_friendly_id? #will change the slug if the name changed
    slug.blank? || name_changed?
  end

  has_many :blog_posts
  has_many :author_links

  def to_s
    @name
  end
  
end
