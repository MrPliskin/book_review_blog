module SubscribersHelper
  def send_all_emails
    subscribers = Subscriber.all
    # recent_posts = BlogPost.where("created_at >= ?", 1.week.ago.utc).order("created_at DESC")

    subscribers.each do |s|
      SubscriptionMailer.send_email(s).deliver_later
    end
  end
end
