class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  protected
    def authenticate_user!
      if user_signed_in?
        super
      else
        redirect_to root_path # new_user_session_path
        ## if you want render 404 page
        ## render :file => File.join(Rails.root, 'public/404'), :formats => [:html], :status => 404, :layout => false
      end
    end

    def authorize_admin
      return unless !current_user.admin?
      redirect_to root_path # , alert: 'Admins only!'
    end
end