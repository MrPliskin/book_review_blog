class UsersController < ApplicationController
  before_filter :authorize_admin, :only => [:create, :new]

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def new
    @user = User.new
  end

  private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :admin)
    end
end