class TagsController < ApplicationController
  def show
    @tag = Tag.friendly.find(params[:id])
    @tag_blog_posts = Tag.friendly.find(params[:id]).blog_posts.paginate(page: params[:page], per_page: 10)
    @tags = Tag.joins(:blog_posts).group("tags.id").order("count(blog_posts.id) DESC")
    
    respond_to do |format|
      format.html
      format.js
    end

    # if user requests old url for post, redirect them to the new url
    if request.path != tag_path(@tag)
      redirect_to @tag, status: :moved_permanently
    end
  end
  
  def index
    # Gets all tags and orders them from most blog posts to least
    @tags = Tag.joins(:blog_posts).group("tags.id").order("count(blog_posts.id) DESC")
  end
end
