class StaticPagesController < ApplicationController
  before_action :authenticate_user!, :except => [:about, :contact]

  def about
    @page = StaticPage.find_or_create_by(name: 'about')
  end

  def contact
    @page = StaticPage.find_or_create_by(name: 'contact')
  end

  def edit
    @page = StaticPage.find(params[:id])
  end

  def update
    @page = StaticPage.find(params[:id])

    if @page.update(static_page_params)
      if @page.name == 'about'
        redirect_to about_path
      elsif @page.name == 'contact'
        redirect_to contact_path
      else
        redirect_to root_path
      end
    else
      render 'edit'
    end
  end

  private
    def static_page_params
      params.require(:static_page).permit(:name, :content)
    end
end
