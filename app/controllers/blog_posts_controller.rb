class BlogPostsController < ApplicationController
  before_action :authenticate_user!, :except => [:index, :show, :browse]

  def index
    # From most recent to least
    # @blog_posts = BlogPost.order("created_at ASC").all

    # per_page is defined in blog post model
    @blog_posts = BlogPost.order('created_at DESC').page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @subscriber = Subscriber.new

    # Note the friendly! It's necessary for use with friendly_id
    @blog_post = BlogPost.friendly.find(params[:id])

    @posted = "Posted: " + @blog_post.created_at.strftime("%B %d, %Y")
    @updated = ""
    if @blog_post.created_at.strftime("%B %d %Y") != @blog_post.updated_at.strftime("%B %d %Y")
      @updated = "Last updated: " + @blog_post.updated_at.strftime("%B %d, %Y")
    end
    # if user requests old url for post, redirect them to the new url
    if request.path != blog_post_path(@blog_post)
      redirect_to @blog_post, status: :moved_permanently
    end
  end

  def new
    @blog_post = BlogPost.new
  end

  def edit
    @blog_post = BlogPost.friendly.find(params[:id])
  end

  def create
    @blog_post = BlogPost.new(blog_post_params)

    if @blog_post.save
      redirect_to @blog_post
    else
      render 'new'
    end
  end

  def update
    @blog_post = BlogPost.friendly.find(params[:id])
    '''
    unless params[:blog_post][:image_url] == ""
      @blog_post.book_cover_from_url(params[:blog_post][:image_url])
    end
    '''

    if @blog_post.update(blog_post_params)
      redirect_to @blog_post
    else
      render 'edit'
    end
  end

  def destroy
    @blog_post = BlogPost.friendly.find(params[:id])
    @blog_post.destroy
 
    redirect_to blog_posts_path
  end

  def browse
  end

  private
    def blog_post_params
      params.require(:blog_post).permit(:title, :book_cover, :image_url, :body, :rating, :tag_list, :author_name) # , :author
    end
end
