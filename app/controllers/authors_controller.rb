class AuthorsController < ApplicationController
  before_action :authenticate_user!, :except => [:index, :show]

  def index
    @authors = Author.joins(:blog_posts).group("authors.id").order("count(blog_posts.id) DESC") # left_outer_joins to include authors with 0 posts
  end

  def show
    @author = Author.friendly.find(params[:id])
    @author_blog_posts = Author.friendly.find(params[:id]).blog_posts.paginate(page: params[:page], per_page: 10)
    @authors = Author.joins(:blog_posts).group("authors.id").order("count(blog_posts.id) DESC")
    
    '''
    respond_to do |format|
      format.html
      format.js
    end
    '''
  end

  def new
    @author = Author.new
  end

  def edit
    @author = Author.friendly.find(params[:id])
  end

  def create
    @author = Author.new(author_params)

    if @author.save
      redirect_to @author
    else
      render 'new'
    end
  end

  def update
    @author = Author.friendly.find(params[:id])

    if @author.update(author_params)
      redirect_to @author
    else
      render 'edit'
    end
  end

  def destroy
    @author = Author.friendly.find(params[:id])
    if @author.blog_posts.count == 0
      @author.destroy
      redirect_to authors_path
    else
      flash.alert = "Error: cannot delete author that is connected to any blog posts"
      redirect_to @author
    end
  end

  private
    def author_params
      params.require(:author).permit(:name, :info)
    end
end
