class SubscribersController < ApplicationController
  def create
    @subscriber = Subscriber.new(subscriber_params)
    @subscriber.save
  end

  def destroy
    email = Rails.application.message_verifier(:token).verify(params[:token])
    @subscriber = Subscriber.find_by(email: email)
    @subscriber.destroy

    flash[:notice] = "Successfully unsubscribed"
    redirect_to root_path
  end

  private
    def subscriber_params
      params.require(:subscriber).permit(:email)
    end
end
