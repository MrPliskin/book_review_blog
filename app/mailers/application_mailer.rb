class ApplicationMailer < ActionMailer::Base
  default from: 'hope@hopespeaksvolumes.com'
  layout 'mailer'
end
