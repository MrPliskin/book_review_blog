class SubscriptionMailer < ApplicationMailer
  def send_email(subscriber)
    @subscriber = subscriber
    # change to created at
    @recent_posts = BlogPost.where("created_at >= ?", 1.week.ago.utc).order("created_at DESC")
    @token = Rails.application.message_verifier(:token).generate(@subscriber.email)
    mail(to: @subscriber.email, subject: "Hope's Weekly Book Review Roundup", content_type: "text/html")
  end
end
