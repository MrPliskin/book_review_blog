# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

listen = ->
  button = $('#email_subscribe_button')
  form = $('#email_form_contents')
  text = $('#email_form_success')

  button.on 'click', (event) ->
    form.fadeOut(500)
    setTimeout (-> text.fadeIn(500)), 500
    return

$(document).ready(listen)
$(document).on('turbolinks:load', listen);