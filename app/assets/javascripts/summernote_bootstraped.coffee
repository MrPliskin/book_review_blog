# Summernote editor
# For more info on installation/customization, see github.com/summernote/summernote-rails

summernote = ->
  $('[data-provider="summernote"]').each ->
    $(this).summernote({
      height: 500
    });

$(document).ready(summernote)
$(document).on('turbolinks:load', summernote);