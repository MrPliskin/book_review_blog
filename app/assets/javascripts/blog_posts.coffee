# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

stars = ->
  $("#rateYo").rateYo({
    rating: $('#rateYo').data('rating')
    halfStar: true
    readOnly: true
    normalFill: "#011826"
    ratedFill: "#049DBF"
    starWidth: "50px"
  });

$(document).ready(stars)
$(document).on('turbolinks:load', stars);